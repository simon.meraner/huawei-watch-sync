start C:\ProgramData\BlueStacks\Client\Bluestacks.exe
PING localhost -n 30 >NUL
adb -s emulator-5554 start-server
adb shell am start -n com.huawei.health/com.huawei.health.MainActivity
PING localhost -n 10 >NUL

adb shell "su -c 'cp /data/data/com.huawei.health/files/HiTrack_* /data/local/tmp/'"
adb shell "su -c 'chown shell.shell /data/local/tmp/HiTrack_*'"
adb pull /data/local/tmp/ temp/

for %%f in (temp/HiTrack_*) do (
    echo %%~nf
    python Huawei-TCX-Converter/Huawei-TCX-Converter.py --file temp/%%~nf --sport Run
)